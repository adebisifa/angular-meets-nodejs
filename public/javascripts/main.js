/**
 * Created by adebisi-fa on 17/02/14.
 */

var module = angular.module("sample-app", ["ngResource"]);

module.controller("MainController", function($scope, $resource) {
    $scope.sampleText = "Make sure things are working.";
    $resource('/get-message').get(
        {},
        function (response) {
            $scope.sampleText = response.message;
            $scope.sampleTextOthers = response.anotherInfo;
        },
        function (error) { }
    );

    $resource('/save-message').save(
        {},
        {
            username: "folsco.net",
            password: "ubuntu-node-angular"
        },
        function (response) {
            $scope.responseObj = response;
        }
    )

    // /save-message/folsco-net-inline/ubuntu-node-angular-secret-inline
    $resource('/save-message/:oruko/:secretKey').get(
        {
            oruko: "folsco-net-inline",
            secretKey: "ubuntu-node-angular-secret-inline"
        },
        null,
        function (response) {
            $scope.responseObj2 = response;
        }
    )
});