
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

app.get("/get-message", function (request, response) {
    response.send({
        message: "[" + new Date().getTime() + "] This is from server.",
        anotherInfo: "Arsenal beats Liverpool at Emirate"
    });
});

app.post("/save-message", function (request, response) {
    response.send({
        username: request.body.username,
        password: request.body.password,
        message: "[" + new Date().getTime() + "] Testing client posting.",
        details: "Arsenal is playing Bayern Munich in Champions League on Wednesday."
    });
});

app.get("/save-message/:username/:password", function (request, response) {
    response.send({
        username: request.params.username,
        password: request.params.password,
        message: "[" + new Date().getTime() + "] Testing client posting.",
        details: "Arsenal is playing Bayern Munich in Champions League on Wednesday."
    });
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
