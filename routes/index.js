
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Express on Azure!' });
};

exports.anotherMethod = function(request, response) {
    response.send("This is cool.");
}